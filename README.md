# OpenML dataset: lungcancer_shedden

https://www.openml.org/d/1245

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Kerby Shedden et al.  
Michel Lang  
**Source**: Unknown - Date unknown  
**Please cite**: Shedden, K., Taylor, J. M. G., Enkemann, S. A., Tsao, M. S., Yeatman, T. J., Gerald, W. L., … Sharma, A. (2008). Gene Expression-Based Survival Prediction in Lung Adenocarcinoma: A Multi-Site, Blinded Validation Study: Director’s Challenge Consortium for the Molecular Classification of Lung Adenocarcinoma. Nature Medicine, 14(8), 822–827. doi:10.1038/nm.1790

fRMA-normalized. Only "Kratz-genes"*.

\* (see: A practical molecular assay to predict survival in resected non-squamous, non-small-cell lung cancer: development and international validation studies
Kratz, Johannes R et al.
The Lancet , Volume 379 , Issue 9818 , 823 - 832)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1245) of an [OpenML dataset](https://www.openml.org/d/1245). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1245/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1245/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1245/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

